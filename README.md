# Nginx Web Server

Server for the web, deployable with Kubernetes.

Simple k8s app deployment example.

```bash
kubectl apply -k .
```
